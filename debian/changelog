ocaml-expat (1.1.0-1) unstable; urgency=low

  [ Stéphane Glondu ]
  * Team upload
  * New upstream release
  * Update Vcs-* to point to salsa
  * Update Homepage and add debian/watch
  * Switch source package format to 3.0 (quilt)
  * Bump debhelper compat level to 13
  * Bump Standards-Version to 4.5.0
  * Add Rules-Requires-Root: no

  [ Ralf Treinen ]
  * Fixed typo in long description (closes: #651553)

 -- Stéphane Glondu <glondu@debian.org>  Sun, 23 Aug 2020 14:51:38 +0200

ocaml-expat (0.9.1+debian1-7) unstable; urgency=low

  [ Stéphane Glondu ]
  * Fix CDBS include order (Closes: #569267)

  [ Enrico Tassi ]
  * Maintainer is now Debian OCaml Maintainers, I'm in Uploaders

 -- Enrico Tassi <gareuselesinge@debian.org>  Sun, 14 Feb 2010 15:52:28 +0100

ocaml-expat (0.9.1+debian1-6) unstable; urgency=low

  [ Stephane Glondu ]
  * Use ocaml.mk as a CDBS "rules" file (Closes: #549715)

  [ Enrico Tassi ]
  * Switch packaging to git
  * ported to new dh-ocaml
  * added debian/libexpat-ocaml-dev.ocamldoc
  * Section: ocaml
  * Fixed VCS-Browse field in control file
  * Bumped standards-version to 3.8.3, no changes
  * debian/compat set to 7, fixed build dependency accordingly
  * fixed copyright file adding years of copyright holding

 -- Enrico Tassi <gareuselesinge@debian.org>  Wed, 11 Nov 2009 17:33:24 +0100

ocaml-expat (0.9.1+debian1-5) unstable; urgency=low

  * Rebuild against 3.11
  * Updated standards-version to 3.8.0, no changes needed
  * Added dh-ocaml do build-depends
  * Adopted @OCamlStdlibDir@ and @OCamlStdlibDir@

 -- Enrico Tassi <gareuselesinge@debian.org>  Mon, 09 Mar 2009 22:27:20 +0100

ocaml-expat (0.9.1+debian1-4) unstable; urgency=low

  [ Stefano Zacchiroli ]
  * fix vcs-svn field to point just above the debian/ dir

  [ Enrico Tassi ]
  * rebuild agains ocaml 3.10.1
  * added Homepage control field
  * bumped standards version to 3.7.3

 -- Enrico Tassi <gareuselesinge@debian.org>  Sat, 09 Feb 2008 16:45:17 +0100

ocaml-expat (0.9.1+debian1-3) unstable; urgency=low

  * rebuilt against 3.10.0
  * moved rules to ocaml CDBS class
  * removed libexpat-ocaml-dev.docs since ocamldoc is now called by the CDBS
    class

 -- Enrico Tassi <gareuselesinge@debian.org>  Mon, 03 Sep 2007 15:29:06 +0100

ocaml-expat (0.9.1+debian1-1) unstable; urgency=low

  * Removed spurious directory from the orig.tgz (Closes: #379793)

 -- Enrico Tassi <gareuselesinge@debian.org>  Fri, 28 Jul 2006 10:22:57 +0200

ocaml-expat (0.9.1-5) unstable; urgency=low

  * rebuilt against ocaml 3.09.2

 -- Enrico Tassi <gareuselesinge@debian.org>  Thu, 18 May 2006 19:30:53 +0200

ocaml-expat (0.9.1-4) unstable; urgency=low

  * rebuilt against ocaml 3.09.1
  * added documentation (Closes: #310491)
  * maintainer email updated to gareuselesinge@debian.org

 -- Enrico Tassi <gareuselesinge@debian.org>  Sat, 14 Jan 2006 11:13:42 +0100

ocaml-expat (0.9.1-3) unstable; urgency=low

  * rebuilt against ocaml 3.09.0
  * moved debian/rules to cdbs
  * no more ocaml-version hardcoded inside any file. @OCamlABI@ is substituted
    in any FILE.in file (except control.in) at build time by the ocamlinit:
    rule producing the corresponding FILE in debian/.

 -- Enrico Tassi <gareuselesinge@users.sourceforge.net>  Sat, 26 Nov 2005 22:15:07 +0100

ocaml-expat (0.9.1-2) unstable; urgency=low

  * no more useless /usr/lib/ocaml/ocaml/3.08.3/stublibs is created
    (closes: Bug#303908)

 -- Enrico Tassi <gareuselesinge@users.sourceforge.net>  Sat,  9 Apr 2005 21:02:42 +0200

ocaml-expat (0.9.1-1) unstable; urgency=low

  * New upstream release
  * Rebuilt against ocaml 3.08.3

 -- Enrico Tassi <gareuselesinge@users.sourceforge.net>  Wed, 29 Mar 2005 11:32:27 +0100

ocaml-expat (0.9.0-1) unstable; urgency=low

  * Initial Release (Closes: Bug#291966).

 -- Enrico Tassi <gareuselesinge@users.sourceforge.net>  Sun, 23 Jan 2005 16:33:21 +0100
